#!/bin/bash -x

# Warning! I made this script for singleplayer & development. For now, not
# expose this server on the internet.

# The name of your nspawn container
CONTAINER=openrsc

# The version of Ubuntu you will be using for your container.
CODENAME=eoan

# The directories for both the containers & the package cache. Editing these may
# result in catastrophic damage to your host system. To work around this,
# symlink another directory to /opt/nspawn
# Example: sudo ln -s /mnt/hdd/nspawn /opt/nspawn
NSPAWN_DIR=/opt/nspawn/machines/${CONTAINER}
CACHE_DIR=${NSPAWN_DIR}/../../cache/ubuntu-${CODENAME}

# The credentals for your user account inside the container.
NSPAWN_USER=user
NSPAWN_PW=password

# The credentals for your container's database.
DB=cabbage
DB_USER=user
DB_PW=password

# Server Ports
MAIN_PORT=43595
DOWNLOAD_PORT=8080
SERVER_PORT=80

# List of dependencies for both the host system & container.
# CONTAINER_DEPS needs commas without spaces between listed packages while
# everything else needs to be wrapped in apostrophes.
ARCH_DEPS='git debootstrap ubuntu-keyring dbeaver'
UBUNTU_DEPS='git debootstrap dbeaver'
DEBIAN_DEPS='git debootstrap ubuntu-keyring dbeaver'

CONTAINER_DEPS=systemd-container
BUILD_DEPS='git wget mc htop nano unzip build-essential apt-transport-https ca-certificates software-properties-common ack curl certbot libswt-gtk-4-java gtk3-nocsd' #gradle
FIREWALL_DEPS='fail2ban'
SERVER_DEPS='mariadb-client mariadb-server screen' #nginx
JAVA_DEPS='openjdk-14-jdk ant'

# Specific commit of OpenRSC 'core' that will be used in this script.
CORE_COMMIT=a8a1109a2634f95ef0b7ed0a6cab3116b3d6ee24
SITE_COMMIT=null

# Client welcome messages, currently unused.
SERVER_NAME_WELCOME="RSC Cabbage (localhost)"
WELCOME_TEXT="Visit runescapeclassic.dev"

# Temp files
TEMP=~/tmp_rsc
SCREENDIR=~/screens

# This script should work with any Linux distro that uses systemd.
# It may also work with Windows 10 via Linux File System Windows, but I have yet
# to test that.
# Add any other OS-dependant functions below this one.
arch_install(){
	sudo pacman -Syu ${ARCH_DEPS}
}

ubuntu_install(){
	sudo apt-update; sudo apt upgrade
	sudo apt install ${UBUNTU_DEPS} -y
}

debian_install(){
	sudo apt-update; sudo apt upgrade
	sudo apt install ${DEBIAN_DEPS} -y
}

nspawn_base_init(){
	sudo mkdir -p ${NSPAWN_DIR}
	sudo mkdir -p ${NSPAWN_DIR}/../../cache/ubuntu-${CODENAME}
	sudo chmod 700 ${NSPAWN_DIR}/../../machines
}

nspawn_install_base(){
	sudo debootstrap --cache-dir=${CACHE_DIR} --include=${CONTAINER_DEPS} --components=main,universe ${CODENAME} ${NSPAWN_DIR} http://archive.ubuntu.com/ubuntu/
	sudo rm ${NSPAWN_DIR}/etc/hostname
	sudo echo "127.0.0.1 localhost" > ${NSPAWN_DIR}/etc/hosts
	sudo echo "127.0.1.1 openrsc" >> ${NSPAWN_DIR}/etc/hosts
	sudo rm ${NSPAWN_DIR}/etc/resolv.conf
	sudo mkdir -p ${NSPAWN_DIR}/etc/skel/screens
	sudo chmod 700 ${NSPAWN_DIR}/etc/skel/screens
	sudo mkdir -p ${NSPAWN_DIR}/etc/skel/static/downloads
	sudo echo "Dir::Cache::Archives /opt/nspawn/cache/ubuntu-${CODENAME};" >> ${NSPAWN_DIR}/etc/apt/apt.conf
	sudo echo "Binary::apt::APT::Keep-Downloaded-Packages true;" >> ${NSPAWN_DIR}/etc/apt/apt.conf
	sleep 5
}

nspawn_install_machine(){
	sudo mkdir -p /etc/systemd/nspawn
	sudo cp -f host-files/openrsc.nspawn /etc/systemd/nspawn/
	sudo cp -f host-files/systemd-nspawn@openrsc.service /etc/systemd/system/
	sudo ln -rsf ${NSPAWN_DIR} /var/lib/machines/openrsc
	sudo machinectl start openrsc
	sleep 5
}


nspawn_make_user(){
	sudo machinectl shell root@openrsc /bin/bash -c '
		useradd -m -s /bin/bash '${NSPAWN_USER}'
		sleep 5
	'
}

#cd core
#git reset --hard '${COMMIT}'
nspawn_copy_sources(){
	sudo machinectl shell user@openrsc /bin/bash -c '
		git clone --depth=1 https://gitlab.com/open-runescape-classic/core.git

	'
}

nspawn_db_init(){
	sudo machinectl shell root@openrsc /bin/bash -c '
		cd /home/user/core/Databases
		mysql -e "CREATE USER '${DB_USER}' IDENTIFIED BY '"'${DB_PW}'"';"
		mysql -e "CREATE DATABASE '${DB}';"
		mysql -e "GRANT ALL privileges ON '${DB}'.* TO '${DB_USER}'@localhost IDENTIFIED BY '"'${DB_PW}'"';"
		sleep 5
	'
}

# Currently cannot install the auctionhouse database due to an error:
#
#mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_auctionhouse.sql
# Right now, this function adds most of the "cabbage" extras.
nspawn_db_upgrade(){
	sudo machinectl shell root@openrsc /bin/bash -c '
		cd /home/user/core/Databases
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < core.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_bank_presets.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_clans.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_custom_items.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_custom_npcs.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_custom_objects.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_equipment_tab.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_harvesting.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_ironman.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_npc_kill_counting.sql
		mysql -u '${DB_USER}' --password='${DB_PW}' '${DB}' < Addons/add_runecraft.sql
	'
}


# Minor changes to orsc-core are needed to launch the server as localhost.
# This will change once I start isolating the container from the host system.
nspawn_mod_source(){
	sudo machinectl shell user@openrsc /bin/bash -c '
		cd ~/core/server
		chmod 700 ant_launcher.sh

		sed -i '"'/	db_user:*/c\	db_user:'${DB_USER}''"' connections.conf
		sed -i '"'/	db_pass:*/c\	db_pass:'${DB_PW}''"' connections.conf

		cp rsccabbage.conf local.conf
		sed -i '"'/	server_name_welcome: */c\ server_name_welcome: RSC Cabbage (localhost)     # MODIFIED'"' local.conf
		sed -i '"'/	welcome_text: */c\	welcome_text: Visit runescapeclassic.dev     # MODIFIED'"' local.conf
		sed -i '"'/	spawn_auction_npcs: */c\	spawn_auction_npcs: false'"' local.conf

		cd ~/core/PC_Launcher/src/com/loader/openrsc
		sed -i '"'/public static final String BASE_URL = */c\public static final String BASE_URL = "'"http://127.0.0.1:8080/"'";'"' Constants.java
		sed -i '"'/public static final String UPDATE_JAR_URL = */c\public static final String UPDATE_JAR_URL = "'"http://127.0.0.1:8080/downloads/OpenRSC.jar"'";'"' Constants.java

		cd ~/core/PC_Launcher/src/com/loader/openrsc/frame/listeners
		sed -i '"'/String ip = */c\    String ip = "'"127.0.0.1"'";'"' ButtonListener.java
	'
}

# The firewall & fail2ban be added later once I start isolating the container
# from the host system.
nspawn_install_deps(){
	sudo machinectl shell root@openrsc /bin/bash -c '
		apt update ; apt upgrade
		apt install '"${BUILD_DEPS} ${SERVER_DEPS} ${JAVA_DEPS}"' -y
	'
}

# THe following 4 build functions can be called independently as needed. Just be
# sure to rebuild the md5sum if recompiling the client and/or launchers.
build_server(){
  sudo machinectl shell user@openrsc /bin/bash -c '
    cd ~/core/server
    ant -f build.xml compile_core
    ant -f build.xml compile_plugins
  '
}

build_client(){
  sudo machinectl shell user@openrsc /bin/bash -c '
    cd ~/core/Client_Base
    ant -f build.xml compile
    yes | cp -arf Cache/. ../../static/downloads
		yes | cp -arf *.jar ../../static/downloads
  '
}

build_android(){
  sudo machinectl shell user@openrsc /bin/bash -c '
    cd ~/core/Android_Client/Open\ RSC\ Android\ Client
    gradle -b build.gradle assembleDebug
    yes | cp -f *.apk ../../static/downloads
  '
}

build_launcher(){
  sudo machinectl shell user@openrsc /bin/bash -c '
    cd ~/core/PC_Launcher
    ant -f build.xml compile
    yes | cp -f *.jar ../../static/downloads
  '
}

# Shamelessly stole this snippet from the ORSC install scripts.
build_md5sum(){
  sudo machinectl shell user@openrsc /bin/bash -c '
    cd ~/static/downloads
    find -type f \( -not -name "MD5.SUM" \) -exec md5sum '{}' \; >MD5.SUM
  '
}

# A simple download server is all that's needed for the ORSC launcher to work
# properly in its current form. Nginx is only needed if you want to install the
# full website alongside the game server.
install_darkhttpd(){
  sudo machinectl shell user@openrsc /bin/bash -c '
    cd ~/
    wget https://unix4lyfe.org/darkhttpd/darkhttpd-1.12.tar.bz2
    tar -xvf darkhttpd-1.12.tar.bz2
    cd darkhttpd-1.12
    make
  '
}

start_dl_server(){
	sudo machinectl shell user@openrsc /bin/bash -c '
		cd ~/darkhttpd-1.12
		./darkhttpd ~/static/ --addr 127.0.0.1 --port '${DOWNLOAD_PORT}' --daemon
	'
}

start_game_server(){
	sudo machinectl shell user@openrsc /bin/bash -c '
		cd core/server
		SCREENDIR=/home/user/screens screen -dmS cabbage ./ant_launcher.sh
	'
}

start_screen(){
	sudo machinectl shell user@openrsc /bin/bash -c '
		SCREENDIR=/home/user/screens screen -r cabbage
	'
}

kill_java(){
	sudo machinectl shell root@openrsc /bin/bash -c '
		killall java
	'
	sudo machinectl shell user@openrsc /bin/bash -c '
		SCREENDIR=/home/user/screens screen -wipe
	'
}

kill_dl_server(){
	sudo machinectl shell root@openrsc /bin/bash -c '
		killall darkhttpd
	'
}

shell_root(){
	sudo machinectl shell root@openrsc
}

shell_user(){
	sudo machinectl shell user@openrsc
}

mc_root(){
	sudo machinectl shell root@openrsc /bin/bash -c '
	mc /home/user
	'
}

mc_user(){
	sudo machinectl shell user@openrsc /bin/bash -c '
	mc ~/
	'
}

shell_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})  Download=http://127.0.0.1:${DOWNLOAD_PORT}/" --title "OpenRSC Shell Menu" --menu "Server must be powered on." 0 40 0 \
		"1" "Midnight Commander (root)" \
		"2" "Midnight commander (user)" \
		"3" "/bin/bash (root)" \
		"4" "/bin/bash (user)" \
		"5" "Main Menu" 2>${TEMP}
	case $(cat ${TEMP}) in
		"1") clear; mc_root; shell_menu
			;;
		"2") clear; mc_user; shell_menu
			;;
		"3") clear; shell_root; shell_menu
			;;
		"4") clear; shell_user; shell_menu
			;;
		"5") main_menu
	esac
}

cabbage_menu(){
dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})  Download=http://127.0.0.1:${DOWNLOAD_PORT}/" --title "OpenRSC Cabbage Addons" --checklist "Select any of the following cabbage addons you want enabled." 0 60 0 \
		"1" "Add auction house" off \
		"2" "Add bank presets" off \
		"3" "Add clans" off \
		"4" "Add custom items" off \
		"5" "Add custom NPCs" off \
		"6" "Add custom objects" off \
		"7" "Add equipment tab" off \
		"8" "Add Harvesting" off \
		"9" "Add Ironman Mode" off \
		"10" "Add NPC kill count" off \
		"11" "Add Runecrafting"  off 2>${TEMP}
}

os_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})  Download=http://127.0.0.1:${DOWNLOAD_PORT}/" --title "OpenRSC Install Menu" --menu "\
		Which distro are you running?" 0 40 0 \
			"1" "Arch Linux" \
			"2" "Ubuntu" \
			"3" "Debian" \
			"4" "Install Menu" 2>${TEMP}

		case $(cat ${TEMP}) in
			"1" ) clear; arch_install; sleep 5; install_menu
				;;
			"2" ) clear; ubuntu_install; sleep 5; install_menu
				;;
			"3" ) clear; debian_install; sleep 5; install_menu
				;;
			"4" ) install_menu
		esac
}

install_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})  Download=http://127.0.0.1:${DOWNLOAD_PORT}/" --title "OpenRSC Install Menu" --menu "\
		The first 3 steps require you to run this script as root, but you wont need a password for the rest \
		of the script if you follow the examples listed inside the provided sudoers file." 0 60 0 \
			"1" "Install host system dependencies" \
			"2" "Install container base" \
			"3" "Install machinectl files on host" \
			"4" "install container dependencies" \
			"5" "Setup user account(s) for the container" \
			"6" "Install OpenRSC sources" \
			"7" "Setup DB & sources" \
			"8" "compile server & launcher" \
			"9" "Download & compile darkhttpd" \
			"10" "Main Menu" 2>${TEMP}

	case $(cat ${TEMP}) in
		"1" ) os_menu
			;;
		"2" ) clear; nspawn_base_init; sleep 5; nspawn_install_base; sleep 5; install_menu
			;;
		"3" ) clear; nspawn_install_machine; sleep 5; install_menu
			;;
		"4" ) clear; nspawn_install_deps; sleep 5; install_menu
			;;
		"5" ) clear; nspawn_make_user; sleep 5; install_menu
			;;
		"6" ) clear; nspawn_copy_sources; sleep 5; install_menu
			;;
		"7" ) clear; nspawn_db_init; sleep 5; nspawn_db_upgrade; sleep 5; nspawn_mod_source; sleep 5; install_menu
			;;
		"8" ) clear; build_server; sleep 5; build_client; sleep 5; build_launcher; sleep 5; build_md5sum; sleep 5; install_menu
			;;
		"9" ) clear; install_darkhttpd; sleep 5; install_menu
			;;
		"10" ) main_menu
	esac
}



server_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})  Download=http://127.0.0.1:${DOWNLOAD_PORT}/" --title "Welcome to OpenRSC" --menu "Server Menu" 0 40 0 \
		"1" "Start Server" \
		"2" "View server screen" \
		"3" "Stop Server" \
		"4" "Start Darkhttpd" \
		"5" "Stop Darkhttpd" \
		"6" "Main Menu" 2>${TEMP}
	case $(cat ${TEMP}) in
		"1" ) clear; kill_java; start_game_server; server_menu
			;;
		"2" ) start_screen; server_menu
			;;
		"3" ) kill_java; server_menu
			;;
		"4" ) clear; start_dl_server; server_menu
			;;
		"5" ) kill_dl_server; server_menu
			;;
		"6" ) main_menu
	esac
}

power_menu(){
	dialog --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})  Download=http://127.0.0.1:${DOWNLOAD_PORT}/" --title "Welcome to OpenRSC" --menu "Power Menu" 0 40 0 \
		"1" "Power on ${CONTAINER}" \
		"2" "Reboot ${CONTAINER}" \
		"3" "Shutdown ${CONTAINER}" \
		"4"  "Main Menu" 2>${TEMP}

	case $(cat ${TEMP}) in
		"1" ) sudo machinectl start openrsc ; power_menu
			;;
		"2" ) sudo machinectl shell root@openrsc /bin/bash -c '
						shutdown -h now
		     '
		     sudo machinectl start ${CONTAINER} ; power_menu
			;;
		"3" ) sudo machinectl shell root@openrsc /bin/bash -c '
			     shutdown -h now
		     '
				 power_menu
			;;
		"4" ) main_menu
	esac
}

main_menu(){
	rm ${TEMP}
	touch ${TEMP}
	dialog  --backtitle "${NSPAWN_DIR} (Ubuntu-${CODENAME})  Download=http://127.0.0.1:${DOWNLOAD_PORT}/" --title "Welcome to Open RuneScape Classic"  --menu "What would you like to do?" 0 40 0\
	    "1" "Power Menu" \
	    "2" "Server Menu" \
	    "3" "Shell Menu" \
	    "4" "Install OpenRSC" \
	    "5" "Exit" 2>${TEMP}

	case $(cat ${TEMP}) in
		"1" ) power_menu
		;;
		"2" ) server_menu
		;;
		"3" ) shell_menu
		;;
		"4" ) install_menu
		;;
		"5" ) rm ${TEMP} ; clear ; exit
	esac


}

#while true
#	do
		main_menu
#	done
